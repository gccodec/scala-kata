package it.gccodec.scalakata.birthdaygreetings.gateway

import it.gccodec.scalakata.birthdaygreetings.core.DateTimeProvider
import it.gccodec.scalakata.birthdaygreetings.model.{Contact, Errors}
import it.gccodec.scalakata.birthdaygreetings.repository.ContactRepository

trait CelebrationsGateway {
  def celebrations(): Either[Errors, Seq[Contact]]
}

object CelebrationsGateway {

  class BirthdayCelebrationsGateway(
      dateTimeProvider: DateTimeProvider,
      repository: ContactRepository
  ) extends CelebrationsGateway {

    override def celebrations(): Either[Errors, Seq[Contact]] =
      for {
        today        <- dateTimeProvider.currentDate()
        celebrations <- repository.findByBirthDate(today)
      } yield celebrations
  }

}
