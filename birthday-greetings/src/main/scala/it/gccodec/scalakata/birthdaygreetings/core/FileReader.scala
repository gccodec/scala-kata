package it.gccodec.scalakata.birthdaygreetings.core

import it.gccodec.scalakata.birthdaygreetings.model.Errors
import it.gccodec.scalakata.birthdaygreetings.model.Errors.{FileNotFoundError, FileReadError}

import scala.io.Source

trait FileReader {
  def read(path: String): Either[Errors, Seq[String]]
}

object FileReader {

  class DefaultFileReader extends FileReader {
    override def read(path: String): Either[Errors, Seq[String]] =
      try {
        val res = Source.fromFile(path)
        try Right(res.getLines().toList)
        catch { case _: Exception => Left(FileReadError) }
        finally if (res != null)
          res.close()
      } catch {
        case _: Exception => Left(FileNotFoundError)
      }
  }
}
