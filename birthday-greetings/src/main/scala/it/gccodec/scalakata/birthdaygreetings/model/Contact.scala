package it.gccodec.scalakata.birthdaygreetings.model

import java.time.LocalDate
import java.time.format.DateTimeFormatter.ofPattern

case class Contact(
    lastName: String,
    firstName: String,
    birthDate: LocalDate,
    email: String
)

object Contact {

  def fromString(csv: String): Contact = {
    val values = csv.split(",")
    Contact(
      lastName = values(0),
      firstName = values(1),
      birthDate = LocalDate.parse(values(2), ofPattern("yyyy/MM/dd")),
      email = values(3)
    )
  }

  implicit class ContactOps(c: Contact) {

    def isBirthday(date: LocalDate): Boolean =
      c.birthDate.isBefore(date) &&
      (
        (c.birthDate.getMonthValue == date.getMonthValue && c.birthDate.getDayOfMonth == date.getDayOfMonth) ||
        (
          (c.birthDate.isLeapYear && !date.isLeapYear) &&
          (
            c.birthDate.getMonthValue == 2 && c.birthDate.getDayOfMonth == 29 &&
            date.getMonthValue == 2 && date.getDayOfMonth == 28
          )
        )
      )

    def fullName: String =
      s"${c.firstName} ${c.lastName}"
  }

}
