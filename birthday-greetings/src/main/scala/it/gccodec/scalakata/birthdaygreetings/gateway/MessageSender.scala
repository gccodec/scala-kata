package it.gccodec.scalakata.birthdaygreetings.gateway

import it.gccodec.scalakata.birthdaygreetings.model.Errors.{
  EmailSendMessageError,
  EmailServerSessionError
}
import it.gccodec.scalakata.birthdaygreetings.model.{Errors, Message}

import java.util.Properties
import javax.mail.Message.RecipientType._
import javax.mail.internet.{InternetAddress, MimeMessage}
import javax.mail.{Session, Transport}

trait MessageSender {
  def send(messages: Seq[Message]): Either[Errors, Unit]
}

object MessageSender {

  class EmailMessageSender(smtpHost: String, smtpPort: Int) extends MessageSender {

    override def send(messages: Seq[Message]): Either[Errors, Unit] =
      for {
        session    <- buildSession()
        sendResult <- sendEmails(session, messages)
      } yield sendResult

    private def buildSession(): Either[Errors, Session] =
      try {
        val props = new Properties()
        props.put("mail.smtp.host", smtpHost)
        props.put("mail.smtp.port", s"$smtpPort")
        Right(Session.getInstance(props))
      } catch {
        case _: Exception => Left(EmailServerSessionError)
      }

    private def sendEmails(session: Session, messages: Seq[Message]): Either[Errors, Unit] =
      try {
        val mimeMessages = messages.map { m =>
          val mimeMessage = new MimeMessage(session)
          mimeMessage.setRecipient(TO, new InternetAddress(m.recipient))
          mimeMessage.setSubject(m.subject)
          mimeMessage.setText(m.content)
          mimeMessage
        }
        Right(mimeMessages.foreach(m => Transport.send(m)))
      } catch { case _: Exception => Left(EmailSendMessageError) }

  }

}
