package it.gccodec.scalakata.birthdaygreetings.model

case class Message(
    sender: String,
    recipient: String,
    subject: String,
    content: String
)
