package it.gccodec.scalakata.birthdaygreetings

import it.gccodec.scalakata.birthdaygreetings.gateway.{CelebrationsGateway, NotificationsGateway}
import it.gccodec.scalakata.birthdaygreetings.model.Errors

class BirthdayGreetingsService(
    celebrationsGateway: CelebrationsGateway,
    notificationsGateway: NotificationsGateway
) {

  def run(): Either[Errors, Unit] =
    for {
      celebrations <- celebrationsGateway.celebrations()
      result       <- notificationsGateway.sendNotification(celebrations)
    } yield result

}
