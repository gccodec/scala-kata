package it.gccodec.scalakata.birthdaygreetings.repository

import it.gccodec.scalakata.birthdaygreetings.core.FileReader
import it.gccodec.scalakata.birthdaygreetings.model.Contact.fromString
import it.gccodec.scalakata.birthdaygreetings.model.{Contact, Errors}

import java.time.LocalDate

trait ContactRepository {
  def findByBirthDate(date: LocalDate): Either[Errors, Seq[Contact]]
}

object ContactRepository {

  class CsvFileContactRepository(fileReader: FileReader, path: String) extends ContactRepository {
    override def findByBirthDate(date: LocalDate): Either[Errors, Seq[Contact]] =
      for {
        lines <- fileReader.read(path)
        contacts = lines.map(fromString)
        birthday = contacts.filter(_.isBirthday(date))
      } yield birthday
  }
}
