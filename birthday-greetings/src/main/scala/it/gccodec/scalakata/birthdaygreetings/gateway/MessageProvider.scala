package it.gccodec.scalakata.birthdaygreetings.gateway

import it.gccodec.scalakata.birthdaygreetings.model.Errors.MessageGenerationError
import it.gccodec.scalakata.birthdaygreetings.model.{Contact, Errors, Message}

trait MessageProvider {

  def get(contacts: Seq[Contact]): Either[Errors, Seq[Message]] =
    try Right(createMessages(contacts))
    catch { case _: Exception => Left(MessageGenerationError) }

  protected def createMessages(contacts: Seq[Contact]): Seq[Message]
}

object MessageProvider {

  abstract class AbstractMessageProvider(sender: String, subject: String, bodyTemplate: String)
      extends MessageProvider {

    override protected def createMessages(contacts: Seq[Contact]): Seq[Message] =
      contactsMessagePart(contacts).map { case (recipient, contactNames) =>
        Message(sender, recipient, subject, bodyTemplate.format(contactNames))
      }

    protected def contactsMessagePart(contacts: Seq[Contact]): Seq[(String, String)]
  }

  class HappyBirthdayMessageProvider(sender: String)
      extends AbstractMessageProvider(sender, "Happy Birthday!", "Happy birthday, dear %s") {

    override protected def contactsMessagePart(contacts: Seq[Contact]): Seq[(String, String)] =
      contacts.map(c => (c.email, c.firstName))
  }

  class HappyBirthdayReminderMessageProvider(sender: String, recipient: String)
      extends AbstractMessageProvider(
        sender,
        "Birthday Reminder",
        "Today is %s's birthday.\nDon't forget to send him a message!"
      ) {

    override protected def contactsMessagePart(contacts: Seq[Contact]): Seq[(String, String)] =
      contacts.map(c => (recipient, c.fullName))

  }

  class SingleHappyBirthdayReminderMessageProvider(sender: String, recipient: String)
      extends HappyBirthdayReminderMessageProvider(sender, recipient) {

    override protected def contactsMessagePart(contacts: Seq[Contact]): Seq[(String, String)] =
      Seq((recipient, contacts.map(c => c.fullName).mkString(", ")))
  }

}
