package it.gccodec.scalakata.birthdaygreetings.core

import it.gccodec.scalakata.birthdaygreetings.model.Errors
import it.gccodec.scalakata.birthdaygreetings.model.Errors.DateTimeError

import java.time.{Clock, LocalDate}

trait DateTimeProvider {
  def currentDate(): Either[Errors, LocalDate]
}

object DateTimeProvider {

  class DefaultDateTimeProvider(clock: Clock) extends DateTimeProvider {
    override def currentDate(): Either[Errors, LocalDate] =
      try Right(LocalDate.now(clock))
      catch { case _: Exception => Left(DateTimeError) }
  }

}
