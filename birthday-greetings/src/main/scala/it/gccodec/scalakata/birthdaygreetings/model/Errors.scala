package it.gccodec.scalakata.birthdaygreetings.model

sealed trait Errors

object Errors {

  case object FileNotFoundError           extends Errors
  case object FileReadError               extends Errors
  case object DateTimeError               extends Errors
  case object RepositoryError             extends Errors
  case object MessageGenerationError      extends Errors
  case object EmailServerSessionError     extends Errors
  case object EmailMessageGenerationError extends Errors
  case object EmailSendMessageError       extends Errors

}
