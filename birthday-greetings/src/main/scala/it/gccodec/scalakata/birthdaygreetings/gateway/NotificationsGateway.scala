package it.gccodec.scalakata.birthdaygreetings.gateway

import it.gccodec.scalakata.birthdaygreetings.model.{Contact, Errors}

trait NotificationsGateway {
  def sendNotification(celebrations: Seq[Contact]): Either[Errors, Unit]
}

object NotificationsGateway {

  class EmailNotificationsGateway(
      messageProvider: MessageProvider,
      messageSender: MessageSender
  ) extends NotificationsGateway {

    override def sendNotification(celebrations: Seq[Contact]): Either[Errors, Unit] =
      for {
        message    <- messageProvider.get(celebrations)
        sendResult <- messageSender.send(message)
      } yield sendResult

  }
}
