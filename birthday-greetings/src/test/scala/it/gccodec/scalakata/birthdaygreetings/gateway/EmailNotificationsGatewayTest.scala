package it.gccodec.scalakata.birthdaygreetings.gateway

import it.gccodec.scalakata.birthdaygreetings.gateway.NotificationsGateway.EmailNotificationsGateway
import it.gccodec.scalakata.birthdaygreetings.model.Errors.{
  EmailSendMessageError,
  EmailServerSessionError,
  MessageGenerationError
}
import it.gccodec.scalakata.birthdaygreetings.model.{Contact, Message}
import org.scalamock.scalatest.MockFactory
import org.scalatest.funsuite.AnyFunSuite

import java.time.LocalDate._

class EmailNotificationsGatewayTest extends AnyFunSuite with MockFactory {

  val emailMessageProvider: MessageProvider = mock[MessageProvider]
  val emailMessageSender: MessageSender     = mock[MessageSender]

  val emailNotificationsGateway =
    new EmailNotificationsGateway(emailMessageProvider, emailMessageSender)

  test("sendNotification return Right()") {
    inSequence(
      (emailMessageProvider.get _)
        .expects(
          Seq(
            Contact("a", "b", of(2020, 1, 1), "d"),
            Contact("x", "y", of(2020, 1, 1), "t")
          )
        )
        .once()
        .returns(Right(Seq(Message("s", "r", "s", "c")))),
      (emailMessageSender.send _)
        .expects(Seq(Message("s", "r", "s", "c")))
        .once()
        .returns(Right())
    )

    val actual = emailNotificationsGateway.sendNotification(
      Seq(
        Contact("a", "b", of(2020, 1, 1), "d"),
        Contact("x", "y", of(2020, 1, 1), "t")
      )
    )
    assert(actual == Right())
  }

  test("sendNotification return Left(MessageGenerationError)") {
    inSequence(
      (emailMessageProvider.get _)
        .expects(
          Seq(
            Contact("a", "b", of(2020, 1, 1), "d"),
            Contact("x", "y", of(2020, 1, 1), "t")
          )
        )
        .once()
        .returns(Left(MessageGenerationError)),
      (emailMessageSender.send _)
        .expects(*)
        .never()
    )

    val actual = emailNotificationsGateway.sendNotification(
      Seq(
        Contact("a", "b", of(2020, 1, 1), "d"),
        Contact("x", "y", of(2020, 1, 1), "t")
      )
    )
    assert(actual == Left(MessageGenerationError))
  }

  test("sendNotification return Left(EmailServerSessionError)") {
    inSequence(
      (emailMessageProvider.get _)
        .expects(
          Seq(
            Contact("a", "b", of(2020, 1, 1), "d"),
            Contact("x", "y", of(2020, 1, 1), "t")
          )
        )
        .once()
        .returns(Right(Seq(Message("s", "r", "s", "c")))),
      (emailMessageSender.send _)
        .expects(Seq(Message("s", "r", "s", "c")))
        .once()
        .returns(Left(EmailServerSessionError))
    )

    val actual = emailNotificationsGateway.sendNotification(
      Seq(
        Contact("a", "b", of(2020, 1, 1), "d"),
        Contact("x", "y", of(2020, 1, 1), "t")
      )
    )
    assert(actual == Left(EmailServerSessionError))
  }

  test("sendNotification return Left(EmailSendMessageError)") {
    inSequence(
      (emailMessageProvider.get _)
        .expects(
          Seq(
            Contact("a", "b", of(2020, 1, 1), "d"),
            Contact("x", "y", of(2020, 1, 1), "t")
          )
        )
        .once()
        .returns(Right(Seq(Message("s", "r", "s", "c")))),
      (emailMessageSender.send _)
        .expects(Seq(Message("s", "r", "s", "c")))
        .once()
        .returns(Left(EmailSendMessageError))
    )

    val actual = emailNotificationsGateway.sendNotification(
      Seq(
        Contact("a", "b", of(2020, 1, 1), "d"),
        Contact("x", "y", of(2020, 1, 1), "t")
      )
    )
    assert(actual == Left(EmailSendMessageError))
  }
}
