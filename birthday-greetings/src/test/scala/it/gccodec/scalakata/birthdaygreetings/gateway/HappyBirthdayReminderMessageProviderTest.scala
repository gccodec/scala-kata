package it.gccodec.scalakata.birthdaygreetings.gateway

import it.gccodec.scalakata.birthdaygreetings.gateway.MessageProvider.HappyBirthdayReminderMessageProvider
import it.gccodec.scalakata.birthdaygreetings.model.{Contact, Message}
import org.scalatest.funsuite.AnyFunSuite

import java.time.LocalDate

class HappyBirthdayReminderMessageProviderTest extends AnyFunSuite {

  test("get return Right(Seq)") {
    val messageProvider =
      new HappyBirthdayReminderMessageProvider("sender", "recipient")

    val actual = messageProvider.get(
      Seq(
        Contact("a", "b", LocalDate.of(2020, 1, 1), "d"),
        Contact("x", "y", LocalDate.of(2020, 1, 1), "t")
      )
    )

    val expected = Right(
      Seq(
        Message(
          sender = "sender",
          recipient = "recipient",
          subject = "Birthday Reminder",
          content = "Today is b a's birthday.\nDon't forget to send him a message!"
        ),
        Message(
          sender = "sender",
          recipient = "recipient",
          subject = "Birthday Reminder",
          content = "Today is y x's birthday.\nDon't forget to send him a message!"
        )
      )
    )

    assert(actual == expected)
  }
}
