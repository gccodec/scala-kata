package it.gccodec.scalakata.birthdaygreetings.gateway

import it.gccodec.scalakata.birthdaygreetings.gateway.MessageProvider.{
  HappyBirthdayMessageProvider,
  HappyBirthdayReminderMessageProvider
}
import it.gccodec.scalakata.birthdaygreetings.model.{Contact, Message}
import org.scalatest.funsuite.AnyFunSuite

import java.time.LocalDate

class HappyBirthdayMessageProviderTest extends AnyFunSuite {

  test("get return Right(Seq)") {
    val messageProvider =
      new HappyBirthdayMessageProvider("sender")

    val actual = messageProvider.get(
      Seq(
        Contact("a", "b", LocalDate.of(2020, 1, 1), "d"),
        Contact("x", "y", LocalDate.of(2020, 1, 1), "t")
      )
    )

    val expected = Right(
      Seq(
        Message(
          sender = "sender",
          recipient = "d",
          subject = "Happy Birthday!",
          content = "Happy birthday, dear b"
        ),
        Message(
          sender = "sender",
          recipient = "t",
          subject = "Happy Birthday!",
          content = "Happy birthday, dear y"
        )
      )
    )

    assert(actual == expected)
  }

}
