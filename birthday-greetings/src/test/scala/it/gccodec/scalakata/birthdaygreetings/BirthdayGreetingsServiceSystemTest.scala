package it.gccodec.scalakata.birthdaygreetings

import com.dumbster.smtp.{SimpleSmtpServer, SmtpMessage}
import it.gccodec.scalakata.birthdaygreetings.core.DateTimeProvider.DefaultDateTimeProvider
import it.gccodec.scalakata.birthdaygreetings.core.FileReader.DefaultFileReader
import it.gccodec.scalakata.birthdaygreetings.gateway.CelebrationsGateway.BirthdayCelebrationsGateway
import it.gccodec.scalakata.birthdaygreetings.gateway.MessageProvider.{
  HappyBirthdayMessageProvider,
  HappyBirthdayReminderMessageProvider,
  SingleHappyBirthdayReminderMessageProvider
}
import it.gccodec.scalakata.birthdaygreetings.gateway.MessageSender.EmailMessageSender
import it.gccodec.scalakata.birthdaygreetings.gateway.NotificationsGateway.EmailNotificationsGateway
import it.gccodec.scalakata.birthdaygreetings.repository.ContactRepository.CsvFileContactRepository
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach}

import java.nio.file.{Files, Paths}
import java.time.Clock._
import java.time.Instant._
import java.time.ZoneOffset._
import scala.collection.JavaConverters.seqAsJavaListConverter
import scala.reflect.io.Path

class BirthdayGreetingsServiceSystemTest
    extends AnyFunSuite
    with BeforeAndAfterEach
    with BeforeAndAfterAll {

  var simpleSmtpServer: SimpleSmtpServer = _
  private val workspace                  = "birthday-greetings/src/test/resources/system-test"

  override protected def beforeAll(): Unit = {
    Path(workspace).deleteRecursively()
    Path(workspace).createDirectory()
  }

  override protected def afterAll(): Unit =
    Path(workspace).deleteRecursively()

  override protected def beforeEach(): Unit = {
    simpleSmtpServer = SimpleSmtpServer.start(502)
    super.beforeEach()
  }

  override protected def afterEach(): Unit = {
    simpleSmtpServer.stop()
    super.afterEach()
  }

  test("run with HappyBirthdayMessageProvider") {
    Files.write(
      Paths.get(s"$workspace/file1.csv"),
      Seq(
        "a,b,1989/07/14,c",
        "d,e,2010/07/14,f",
        "g,h,1988/02/11,i",
        "l,m,1987/03/20,n",
        "o,p,1999/10/30,q"
      ).asJava
    )

    val actual = new BirthdayGreetingsService(
      new BirthdayCelebrationsGateway(
        new DefaultDateTimeProvider(fixed(parse("2020-07-14T00:00:00.00Z"), UTC)),
        new CsvFileContactRepository(new DefaultFileReader(), s"$workspace/file1.csv")
      ),
      new EmailNotificationsGateway(
        new HappyBirthdayMessageProvider(
          sender = "sender@sender.com"
        ),
        new EmailMessageSender("localhost", 502)
      )
    ).run()

    assert(actual == Right())

    val receivedEmail = simpleSmtpServer.getReceivedEmail
    val email1        = receivedEmail.next().asInstanceOf[SmtpMessage]
    val email2        = receivedEmail.next().asInstanceOf[SmtpMessage]

    assert(email1.getHeaderValue("To") == "c")
    assert(email1.getHeaderValue("Subject") == "Happy Birthday!")
    assert(email1.getBody == "Happy birthday, dear b")

    assert(email2.getHeaderValue("To") == "f")
    assert(email2.getHeaderValue("Subject") == "Happy Birthday!")
    assert(email2.getBody == "Happy birthday, dear e")

  }

  test("run with MultiBirthdayReminderMessageProvider") {
    Files.write(
      Paths.get(s"$workspace/file2.csv"),
      Seq(
        "a,b,1989/07/14,c",
        "d,e,2010/07/14,f",
        "g,h,1988/02/11,i",
        "l,m,1987/03/20,n",
        "o,p,1999/10/30,q"
      ).asJava
    )

    val actual = new BirthdayGreetingsService(
      new BirthdayCelebrationsGateway(
        new DefaultDateTimeProvider(fixed(parse("2020-07-14T00:00:00.00Z"), UTC)),
        new CsvFileContactRepository(new DefaultFileReader(), s"$workspace/file2.csv")
      ),
      new EmailNotificationsGateway(
        new HappyBirthdayReminderMessageProvider(
          sender = "sender@sender.com",
          recipient = "recipient@recipient.com"
        ),
        new EmailMessageSender("localhost", 502)
      )
    ).run()

    assert(actual == Right())

    val receivedEmail = simpleSmtpServer.getReceivedEmail
    val email1        = receivedEmail.next().asInstanceOf[SmtpMessage]
    val email2        = receivedEmail.next().asInstanceOf[SmtpMessage]

    assert(email1.getHeaderValue("To") == "recipient@recipient.com")
    assert(email1.getHeaderValue("Subject") == "Birthday Reminder")
    assert(email1.getBody == "Today is b a's birthday.Don't forget to send him a message!")

    assert(email2.getHeaderValue("To") == "recipient@recipient.com")
    assert(email2.getHeaderValue("Subject") == "Birthday Reminder")
    assert(email2.getBody == "Today is e d's birthday.Don't forget to send him a message!")
  }

  test("run with SingleBirthdayReminderMessageProvider") {
    Files.write(
      Paths.get(s"$workspace/file3.csv"),
      Seq(
        "a,b,1989/07/14,c",
        "d,e,2010/07/14,f",
        "g,h,1988/02/11,i",
        "l,m,1987/03/20,n",
        "o,p,1999/10/30,q"
      ).asJava
    )

    val actual = new BirthdayGreetingsService(
      new BirthdayCelebrationsGateway(
        new DefaultDateTimeProvider(fixed(parse("2020-07-14T00:00:00.00Z"), UTC)),
        new CsvFileContactRepository(new DefaultFileReader(), s"$workspace/file3.csv")
      ),
      new EmailNotificationsGateway(
        new SingleHappyBirthdayReminderMessageProvider(
          sender = "sender@sender.com",
          recipient = "recipient@recipient.com"
        ),
        new EmailMessageSender("localhost", 502)
      )
    ).run()

    assert(actual == Right())

    val receivedEmail = simpleSmtpServer.getReceivedEmail
    val email1        = receivedEmail.next().asInstanceOf[SmtpMessage]

    assert(email1.getHeaderValue("To") == "recipient@recipient.com")
    assert(email1.getHeaderValue("Subject") == "Birthday Reminder")
    assert(email1.getBody == "Today is b a, e d's birthday.Don't forget to send him a message!")
  }

}
