package it.gccodec.scalakata.birthdaygreetings

import it.gccodec.scalakata.birthdaygreetings.gateway.{CelebrationsGateway, NotificationsGateway}
import it.gccodec.scalakata.birthdaygreetings.model.Contact
import it.gccodec.scalakata.birthdaygreetings.model.Errors.{
  DateTimeError,
  EmailSendMessageError,
  RepositoryError
}
import org.scalamock.scalatest.MockFactory
import org.scalatest.funsuite.AnyFunSuite

import java.time.LocalDate

class BirthdayGreetingsServiceTest extends AnyFunSuite with MockFactory {

  val celebrationsGateway: CelebrationsGateway  = mock[CelebrationsGateway]
  val notificationService: NotificationsGateway = mock[NotificationsGateway]

  val birthdayGreetingsService =
    new BirthdayGreetingsService(celebrationsGateway, notificationService)

  test("run return Right(Unit)") {
    inSequence(
      (celebrationsGateway.celebrations _)
        .expects()
        .once()
        .returns(
          Right(
            Seq(
              Contact("a", "b", LocalDate.of(2020, 1, 1), "d"),
              Contact("e", "f", LocalDate.of(2020, 1, 1), "h")
            )
          )
        ),
      (notificationService.sendNotification _)
        .expects(
          Seq(
            Contact("a", "b", LocalDate.of(2020, 1, 1), "d"),
            Contact("e", "f", LocalDate.of(2020, 1, 1), "h")
          )
        )
        .once()
        .returns(Right())
    )
    val actual = birthdayGreetingsService.run()
    assert(actual == Right())
  }

  test("run return Left(DateTimeError)") {
    inSequence(
      (celebrationsGateway.celebrations _)
        .expects()
        .once()
        .returns(Left(DateTimeError)),
      (notificationService.sendNotification _)
        .expects(*)
        .never()
    )
    val actual = birthdayGreetingsService.run()
    assert(actual == Left(DateTimeError))
  }

  test("run return Left(RepositoryError)") {
    inSequence(
      (celebrationsGateway.celebrations _)
        .expects()
        .once()
        .returns(Left(RepositoryError)),
      (notificationService.sendNotification _)
        .expects(*)
        .never()
    )
    val actual = birthdayGreetingsService.run()
    assert(actual == Left(RepositoryError))
  }

  test("run return Left(EmailSendMessageError)") {
    inSequence(
      (celebrationsGateway.celebrations _)
        .expects()
        .once()
        .returns(
          Right(
            Seq(
              Contact("a", "b", LocalDate.of(2020, 1, 1), "d"),
              Contact("e", "f", LocalDate.of(2020, 1, 1), "h")
            )
          )
        ),
      (notificationService.sendNotification _)
        .expects(
          Seq(
            Contact("a", "b", LocalDate.of(2020, 1, 1), "d"),
            Contact("e", "f", LocalDate.of(2020, 1, 1), "h")
          )
        )
        .once()
        .returns(Left(EmailSendMessageError))
    )
    val actual = birthdayGreetingsService.run()
    assert(actual == Left(EmailSendMessageError))
  }

}
