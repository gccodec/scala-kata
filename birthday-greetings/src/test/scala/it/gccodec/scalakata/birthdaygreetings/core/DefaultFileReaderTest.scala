package it.gccodec.scalakata.birthdaygreetings.core

import it.gccodec.scalakata.birthdaygreetings.core.FileReader.DefaultFileReader
import it.gccodec.scalakata.birthdaygreetings.model.Errors.FileNotFoundError
import org.scalatest.BeforeAndAfterAll
import org.scalatest.funsuite.AnyFunSuite

import java.nio.file.{Files, Paths}
import scala.collection.JavaConverters.seqAsJavaListConverter
import scala.reflect.io.Path

class DefaultFileReaderTest extends AnyFunSuite with BeforeAndAfterAll {

  private val workspace = "birthday-greetings/src/test/resources"

  override protected def beforeAll(): Unit = {
    Path(workspace).deleteRecursively()
    Path(workspace).createDirectory()
  }

  override protected def afterAll(): Unit =
    Path(workspace).deleteRecursively()

  test("read return Left(FileNotFound)") {
    val file = new DefaultFileReader().read(s"$workspace/nonExistingFile.txt")
    assert(file == Left(FileNotFoundError))
  }

  test("read return Right(Seq.empty)") {
    Files.write(Paths.get(s"$workspace/empty.txt"), Seq.empty.asJava)
    val file = new DefaultFileReader().read(s"$workspace/empty.txt")
    assert(file == Right(Seq.empty))
  }

  test("read return Right(Seq(a,b,c))") {
    Files.write(Paths.get(s"$workspace/file1.txt"), Seq("a", "b", "c").asJava)
    val file = new DefaultFileReader().read(s"$workspace/file1.txt")
    assert(file == Right(Seq("a", "b", "c")))
  }

}
