package it.gccodec.scalakata.birthdaygreetings.gateway

import it.gccodec.scalakata.birthdaygreetings.core.DateTimeProvider
import it.gccodec.scalakata.birthdaygreetings.gateway.CelebrationsGateway.BirthdayCelebrationsGateway
import it.gccodec.scalakata.birthdaygreetings.model.Contact
import it.gccodec.scalakata.birthdaygreetings.model.Errors.{DateTimeError, RepositoryError}
import it.gccodec.scalakata.birthdaygreetings.repository.ContactRepository
import org.scalamock.scalatest.MockFactory
import org.scalatest.funsuite.AnyFunSuite

import java.time.Clock._
import java.time.LocalDate._
import java.time.{Instant, LocalDate, ZoneOffset}

class BirthdayCelebrationsGatewayTest extends AnyFunSuite with MockFactory {

  val fixedDate: LocalDate                 = now(fixed(Instant.parse("2020-01-01T00:00:00.00Z"), ZoneOffset.UTC))
  val dateTimeProvider: DateTimeProvider   = mock[DateTimeProvider]
  val contactRepository: ContactRepository = mock[ContactRepository]

  val birthdayCelebrationsGateway =
    new BirthdayCelebrationsGateway(dateTimeProvider, contactRepository)

  test("celebrations return Right(Seq(Contact))") {
    inSequence(
      (dateTimeProvider.currentDate _)
        .expects()
        .once()
        .returns(Right(fixedDate)),
      (contactRepository.findByBirthDate _)
        .expects(fixedDate)
        .once()
        .returns(
          Right(
            Seq(
              Contact("x", "x", of(2020, 1, 1), "x"),
              Contact("y", "y", of(2020, 1, 1), "y")
            )
          )
        )
    )

    val actual   = birthdayCelebrationsGateway.celebrations()
    val expected = Right(
      Seq(
        Contact("x", "x", of(2020, 1, 1), "x"),
        Contact("y", "y", of(2020, 1, 1), "y")
      )
    )

    assert(actual == expected)
  }

  test("celebrations return Right(Seq.empty)") {
    inSequence(
      (dateTimeProvider.currentDate _)
        .expects()
        .once()
        .returns(Right(fixedDate)),
      (contactRepository.findByBirthDate _)
        .expects(fixedDate)
        .once()
        .returns(Right(Seq.empty))
    )
    val actual   = birthdayCelebrationsGateway.celebrations()
    val expected = Right(Seq.empty)

    assert(actual == expected)
  }

  test("celebrations return DateTimeError") {
    inSequence(
      (dateTimeProvider.currentDate _)
        .expects()
        .once()
        .returns(Left(DateTimeError)),
      (contactRepository.findByBirthDate _)
        .expects(*)
        .never()
    )

    val actual   = birthdayCelebrationsGateway.celebrations()
    val expected = Left(DateTimeError)

    assert(actual == expected)
  }

  test("celebrations return RepositoryError") {
    inSequence(
      (dateTimeProvider.currentDate _)
        .expects()
        .once()
        .returns(Right(fixedDate)),
      (contactRepository.findByBirthDate _)
        .expects(fixedDate)
        .returns(Left(RepositoryError))
    )

    val actual   = birthdayCelebrationsGateway.celebrations()
    val expected = Left(RepositoryError)

    assert(actual == expected)
  }

}
