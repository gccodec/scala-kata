package it.gccodec.scalakata.birthdaygreetings.repository

import it.gccodec.scalakata.birthdaygreetings.core.FileReader
import it.gccodec.scalakata.birthdaygreetings.model.Contact
import it.gccodec.scalakata.birthdaygreetings.repository.ContactRepository.CsvFileContactRepository
import org.scalamock.scalatest.MockFactory
import org.scalatest.funsuite.AnyFunSuite

import java.time.LocalDate

class ContactRepositoryTest extends AnyFunSuite with MockFactory {

  val fileReader: FileReader = mock[FileReader]
  val repository             = new CsvFileContactRepository(fileReader, "myPath")

  test("return Right(Seq.empty) with empty file") {
    val localDate = LocalDate.of(2020, 1, 2)
    (fileReader.read _).expects("myPath").once().returns(Right(Seq.empty))
    val actual    = repository.findByBirthDate(localDate)
    assert(actual == Right(Seq.empty[Contact]))
  }

  test("return Right(Seq.empty) with no birthday") {
    val localDate = LocalDate.of(2020, 1, 2)
    (fileReader.read _)
      .expects("myPath")
      .once()
      .returns(
        Right(
          Seq(
            "a,b,1989/01/10,d",
            "a,b,1988/02/11,d",
            "a,b,1987/03/20,d",
            "a,b,1999/10/30,d"
          )
        )
      )
    val actual    = repository.findByBirthDate(localDate)
    assert(actual == Right(Seq.empty[Contact]))
  }

  test("return Right(Seq(Contact)) with birthday") {
    val localDate = LocalDate.of(2020, 1, 2)
    (fileReader.read _)
      .expects("myPath")
      .once()
      .returns(
        Right(
          Seq(
            "a,b,1999/01/02,d",
            "a,b,2000/01/02,d",
            "a,b,1987/03/20,d",
            "a,b,1999/10/30,d"
          )
        )
      )
    val actual    = repository.findByBirthDate(localDate)
    val expected  = Right(
      Seq(
        Contact("a", "b", LocalDate.of(1999, 1, 2), "d"),
        Contact("a", "b", LocalDate.of(2000, 1, 2), "d")
      )
    )
    assert(actual == expected)
  }

}
