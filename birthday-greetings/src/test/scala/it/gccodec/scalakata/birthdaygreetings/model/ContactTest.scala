package it.gccodec.scalakata.birthdaygreetings.model

import org.scalatest.funsuite.AnyFunSuite

import java.time.LocalDate.of

class ContactTest extends AnyFunSuite {

  val fromStringTestCases = Seq(
    ("a,b,1982/10/08,d", Contact("a", "b", of(1982, 10, 8), "d")),
    ("e,f,2010/12/01,h", Contact("e", "f", of(2010, 12, 1), "h")),
    ("a,b,2020/05/28,d,e", Contact("a", "b", of(2020, 5, 28), "d"))
  )

  val isBirthdayTestCases = Seq(
    (of(2016, 2, 29), Contact("a", "b", of(2016, 2, 29), "d"), false),
    (of(2017, 2, 10), Contact("a", "b", of(2016, 2, 29), "d"), false),
    (of(2018, 2, 28), Contact("a", "b", of(2016, 2, 29), "d"), true),
    (of(2020, 2, 29), Contact("a", "b", of(2016, 2, 29), "d"), true),
    (of(2020, 2, 28), Contact("a", "b", of(2016, 2, 29), "d"), false),
    (of(2019, 3, 1), Contact("a", "b", of(2016, 3, 1), "d"), true)
  )

  val fullNameTestCases = Seq(
    (Contact("a", "b", of(1982, 10, 8), "d"), "b a"),
    (Contact("e", "f", of(2010, 12, 1), "h"), "f e")
  )

  fromStringTestCases foreach { case (line, expected) =>
    test(s"fromCsv parse $line") {
      assert(Contact.fromString(line) == expected)
    }
  }

  isBirthdayTestCases foreach { case (date, contact, isBirthdayExpected) =>
    test(s"isBirthday of $contact on $date return $isBirthdayExpected") {
      assert(contact.isBirthday(date) == isBirthdayExpected)
    }
  }

  fullNameTestCases foreach { case (contact, expected) =>
    test(s"fullName of $contact ruturn $expected") {
      assert(contact.fullName == expected)
    }
  }

}
