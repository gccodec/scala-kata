package it.gccodec.scalakata.birthdaygreetings.gateway

import com.dumbster.smtp.{SimpleSmtpServer, SmtpMessage}
import it.gccodec.scalakata.birthdaygreetings.gateway.MessageSender.EmailMessageSender
import it.gccodec.scalakata.birthdaygreetings.model.Errors.EmailSendMessageError
import it.gccodec.scalakata.birthdaygreetings.model.Message
import org.scalatest.BeforeAndAfterEach
import org.scalatest.funsuite.AnyFunSuite

class EmailMessageSenderTest extends AnyFunSuite with BeforeAndAfterEach {

  var simpleSmtpServer: SimpleSmtpServer = _

  override protected def beforeEach(): Unit = {
    simpleSmtpServer = SimpleSmtpServer.start(501)
    super.beforeEach()
  }

  override protected def afterEach(): Unit = {
    simpleSmtpServer.stop()
    super.afterEach()
  }

  test("send single message return Right()") {
    val emailMessageSender = new EmailMessageSender("localhost", 501)
    val actual             = emailMessageSender.send(
      Seq(
        Message(
          sender = "sender@mysender.com",
          recipient = "recipient@recipient.com",
          subject = "subject",
          content = "my greetings body"
        )
      )
    )

    val receivedEmail  = simpleSmtpServer.getReceivedEmail
    val receivedEmail1 = receivedEmail.next().asInstanceOf[SmtpMessage]
    assert(actual == Right())
    assert(receivedEmail1.getHeaderValue("To") == "recipient@recipient.com")
    assert(receivedEmail1.getHeaderValue("Subject") == "subject")
    assert(receivedEmail1.getBody == "my greetings body")
  }

  test("send multiple messages Right()") {
    val emailMessageSender = new EmailMessageSender("localhost", 501)
    val actual             = emailMessageSender.send(
      Seq(
        Message(
          sender = "sender@mysender.com",
          recipient = "recipient1@recipient.com",
          subject = "subject1",
          content = "my greetings body 1"
        ),
        Message(
          sender = "sender@mysender.com",
          recipient = "recipient2@recipient.com",
          subject = "subject2",
          content = "my greetings body 2"
        )
      )
    )
    val receivedEmail      = simpleSmtpServer.getReceivedEmail
    val receivedEmail1     = receivedEmail.next().asInstanceOf[SmtpMessage]
    val receivedEmail2     = receivedEmail.next().asInstanceOf[SmtpMessage]

    assert(actual == Right())
    assert(receivedEmail1.getHeaderValue("To") == "recipient1@recipient.com")
    assert(receivedEmail1.getHeaderValue("Subject") == "subject1")
    assert(receivedEmail1.getBody == "my greetings body 1")
    assert(receivedEmail2.getHeaderValue("To") == "recipient2@recipient.com")
    assert(receivedEmail2.getHeaderValue("Subject") == "subject2")
    assert(receivedEmail2.getBody == "my greetings body 2")
  }

  test("send multiple messages Left(EmailSendMessageError)") {
    val emailMessageSender = new EmailMessageSender("myhost", 501)
    val actual             = emailMessageSender.send(
      Seq(
        Message(
          sender = "sender@mysender.com",
          recipient = "recipient1@recipient.com",
          subject = "subject1",
          content = "my greetings body 1"
        ),
        Message(
          sender = "sender@mysender.com",
          recipient = "recipient2@recipient.com",
          subject = "subject2",
          content = "my greetings body 2"
        )
      )
    )
    assert(actual == Left(EmailSendMessageError))
  }

}
