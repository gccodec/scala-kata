package it.gccodec.scalakata.birthdaygreetings.core

import it.gccodec.scalakata.birthdaygreetings.core.DateTimeProvider.DefaultDateTimeProvider
import org.scalatest.funsuite.AnyFunSuite

import java.time.Clock._
import java.time.ZoneOffset._
import java.time.{Instant, LocalDate}

class DefaultDateTimeProviderTest extends AnyFunSuite {

  val testCases = Seq(
    "2020-01-01T00:00:00.00Z",
    "2021-02-02T01:03:03.00Z",
    "2022-03-03T02:04:04.00Z"
  )

  testCases foreach { dateString =>
    test(s"get return Right($dateString)") {
      val clock    = fixed(Instant.parse(dateString), UTC)
      val actual   = new DefaultDateTimeProvider(clock).currentDate()
      val expected = LocalDate.now(clock)
      assert(actual == Right(expected))
    }

  }

}
