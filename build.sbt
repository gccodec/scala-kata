ThisBuild / organization := "it.gccodec"
ThisBuild / version      := "0.1"
ThisBuild / scalaVersion := "2.12.13"

lazy val dependencies = new {

  val javaxMailV = "1.4.7"
  val scalaTestV = "3.2.10"
  val scalaMockV = "5.1.0"
  val dumbsterV  = "1.6"

  val javaxMail = "javax.mail"     % "mail"      % javaxMailV
  val scalactic = "org.scalactic" %% "scalactic" % scalaTestV
  val scalatest = "org.scalatest" %% "scalatest" % scalaTestV
  val scalaMock = "org.scalamock" %% "scalamock" % scalaMockV
  val dumbster  = "dumbster"       % "dumbster"  % dumbsterV

  val testDependencies = Seq(scalactic % "test", scalatest % "test", scalaMock % "test")
}

lazy val root = (project in file("."))
  .aggregate(birthdayGreetings)

lazy val birthdayGreetings = (project in file("birthday-greetings"))
  .settings(
    name := "birthday-greetings",
    libraryDependencies ++= dependencies.testDependencies ++ Seq(
      dependencies.javaxMail,
      dependencies.dumbster % "test"
    )
  )
